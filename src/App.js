import React from "react";
import MessageList from "./components/MessageList";
import MessageActions from "./components/MessageActions";

export default function App() {
  return (
    <div data-testid="APP-TEST-ID" className="App">
      <MessageActions></MessageActions>
      <MessageList></MessageList>
    </div>
  );
}
