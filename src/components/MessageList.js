import React from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import Message from "./Message";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    margin: "5px 0",
  },
}));

const MessageList = () => {
  const messages = useSelector((state) => state.messages);
  const classes = useStyles();

  const renderMessages = (items) =>
    items.map((message, index) => (
      <Message key={index} message={message}></Message>
    ));

  const filteredMessages = (index) =>
    messages.list.filter((message) => message.priority === index);

  return (
    <Container data-testid="MESSAGE-LIST-TEST-ID" className={classes.root}>
      <Grid justifyContent="center" container spacing={0}>
        {messages.list.length > 0 &&
          [1, 2, 3].map((value) => (
            <Grid key={value} xs={4} item>
              <h2 className={classes.title}>
                {value === 1 ? "Error" : value === 2 ? "Warning" : "Info"} Type{" "}
                {value}
              </h2>

              <span>Count {filteredMessages(value).length}</span>

              {filteredMessages(value).length > 0 &&
                renderMessages(filteredMessages(value).reverse())}
            </Grid>
          ))}
        {messages.list.length === 0 && (
          <Typography variant="body1" component="p">
            No messages recieved yet!!!
          </Typography>
        )}
      </Grid>
    </Container>
  );
};

export default MessageList;
