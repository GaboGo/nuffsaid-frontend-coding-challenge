import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { palette } from "./../constants/colors";
import Tooltip from "@material-ui/core/Tooltip";
import { removeMessage } from "../actions/messages";

const useStyles = makeStyles({
  root: {
    maxWidth: 400,
    height: 100,
    color: palette.text,
    margin: "10px 0",
  },
  error: {
    backgroundColor: palette.error,
  },
  warning: {
    backgroundColor: palette.warning,
  },
  info: {
    backgroundColor: palette.info,
  },
  messageAction: {
    marginLeft: "auto",
    textTransform: "none",
  },
  messageBody: {
    width: 360,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
});

const Message = ({ message }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const onRemoveMessage = () => {
    dispatch(removeMessage(message.id));
  };

  return (
    <Card
      data-testid="MESSAGE-TEST-ID"
      className={`${classes.root} ${
        message.priority === 1
          ? classes.error
          : message.priority === 2
          ? classes.warning
          : classes.info
      }`}
    >
      <CardContent>
        {message.message.length > 50 ? (
          <Tooltip title={message.message} placement="top-start">
            <Typography
              className={classes.messageBody}
              variant="body2"
              component="p"
            >
              {message.message}
            </Typography>
          </Tooltip>
        ) : (
          <Typography
            className={classes.messageBody}
            variant="body2"
            component="p"
          >
            {message.message}
          </Typography>
        )}
      </CardContent>
      <CardActions>
        <Button
          onClick={onRemoveMessage}
          className={classes.messageAction}
          size="small"
        >
          Clear
        </Button>
      </CardActions>
    </Card>
  );
};

export default Message;
