import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import Api from "../services/api";
import {
  startMessages,
  addMessage,
  stopMessages,
  clearMessages,
} from "../actions/messages";
import { palette } from "./../constants/colors";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    textAlign: "center",
    marginBottom: 50,
  },
  button: {
    margin: 3,
    backgroundColor: palette.info,
    width: 60,
    height: 30,
    fontWeight: 600,
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const MessageActions = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const messages = useSelector((state) => state.messages);
  const [state, setState] = useState({
    showSnack: false,
    snackMessage: "",
    api: new Api({
      messageCallback: (message) => {
        messageCallback(message);
      },
    }),
  });

  useEffect(() => {
    state.api.start();
    dispatch(startMessages());
  }, []);

  const messageCallback = useCallback((message) => {
    if (message.priority === 1) {
      setState((prevState) => ({
        ...prevState,
        showSnack: true,
        snackMessage: message.message,
      }));
    }
    dispatch(addMessage(message));
  }, []);

  const stopStartClick = () => {
    if (messages.isApiStarted) {
      state.api.stop();
      dispatch(stopMessages());
    } else {
      state.api.start();
      dispatch(startMessages());
    }
  };

  const clearClick = () => {
    state.api.stop();
    dispatch(stopMessages());
    dispatch(clearMessages());
  };

  const handleClose = () => {
    setState((prevState) => ({
      ...prevState,
      showSnack: false,
    }));
  };

  const renderButtons = () => {
    return (
      <Container data-testid="MESSAGE-ACTIONS-TEST-ID" className={classes.root}>
        <Button
          className={classes.button}
          variant="contained"
          onClick={stopStartClick}
        >
          {messages.isApiStarted ? "Stop" : "Start"}
        </Button>
        {messages.list.length > 0 && (
          <Button
            className={classes.button}
            variant="contained"
            onClick={clearClick}
          >
            Clear
          </Button>
        )}
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "left",
          }}
          open={state.showSnack}
          onClose={handleClose}
          key="snackdown"
          autoHideDuration={2000}
        >
          <Alert onClose={handleClose} severity="error">
            Error Recieved: {state.snackMessage}
          </Alert>
        </Snackbar>
      </Container>
    );
  };

  return renderButtons();
};

export default MessageActions;
