import {
  START_MESSAGES,
  STOP_MESSAGES,
  CLEAR_MESSAGES,
  ADD_MESSAGE,
  REMOVE_MESSAGE,
} from "../constants/actionTypes";

import initialState from "./initialState";

export default function messagesReducer(
  state = initialState().messages,
  action
) {
  let list, message, timestamp;
  switch (action.type) {
    case START_MESSAGES:
      return Object.assign({}, state, {
        isApiStarted: true,
      });
      break;
    case ADD_MESSAGE:
      list = state.list;
      timestamp = Math.round(+new Date() / 1000);
      message = { ...action.message, id: timestamp };
      return Object.assign({}, state, {
        list: [...list, message],
      });
      break;
    case STOP_MESSAGES:
      return Object.assign({}, state, {
        isApiStarted: false,
      });
      break;
    case CLEAR_MESSAGES:
      list = state.list;
      return Object.assign({}, state, {
        list: [],
      });
      break;
    case REMOVE_MESSAGE:
      list = state.list.filter((item) => item.id != action.id);
      return { ...state, list };
      break;
    default:
      return state;
      break;
  }
}
