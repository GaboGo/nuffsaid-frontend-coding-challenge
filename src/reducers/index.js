import { combineReducers } from "redux";
import messages from "./messages";

const rootReducers = combineReducers({ messages });

export default rootReducers;
