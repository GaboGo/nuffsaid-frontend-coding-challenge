import * as types from "../constants/actionTypes";

export const startMessages = () => {
  return { type: types.START_MESSAGES };
};

export const stopMessages = () => {
  return { type: types.STOP_MESSAGES };
};

export const clearMessages = () => {
  return { type: types.CLEAR_MESSAGES };
};

export const removeMessage = (id) => {
  return { type: types.REMOVE_MESSAGE, id };
};

export const addMessage = (message) => {
  return { type: types.ADD_MESSAGE, message };
};
