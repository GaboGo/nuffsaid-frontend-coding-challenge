export const palette = {
  text: "#000000",
  error: "#F56236",
  warning: "#FCE788",
  info: "#88FCA3",
};
