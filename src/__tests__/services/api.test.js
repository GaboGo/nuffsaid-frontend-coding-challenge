import Api from "../../services/api";

let msg = undefined;

describe("Api", () => {
  test("start generation", () => {
    const api = new Api({
      messageCallback: (message) => {
        msg = message;
      },
    });
    api.start();
    expect(api.isStarted()).toEqual(true);
    expect(msg.message).toBeDefined();
  });

  test("stop generation", () => {
    const api = new Api({
      messageCallback: () => {},
    });
    api.stop();
    expect(api.isStarted()).toEqual(false);
    expect(api.generate()).toBeUndefined();
  });
});
