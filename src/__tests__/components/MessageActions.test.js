import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { render, screen } from "@testing-library/react";
import MessageActions from "../../components/MessageActions";
import { Provider } from "react-redux";
import configureStore from "../../store/configureStore";

const store = configureStore();

describe("MessageActions", () => {
  test("renders", () => {
    render(
      <Provider store={store}>
        <MessageActions />
      </Provider>
    );
    const linkElement = screen.getByTestId(/MESSAGE-ACTIONS-TEST-ID/i);
    expect(linkElement).toBeInTheDocument();
  });

  test("messages api started by default", () => {
    const state = store.getState();
    expect(state.messages.isApiStarted).toEqual(true);
  });
});
