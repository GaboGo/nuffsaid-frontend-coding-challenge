import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { render, screen } from "@testing-library/react";
import MessageList from "../../components/MessageList";
import { Provider } from "react-redux";
import configureStore from "../../store/configureStore";

const store = configureStore();

describe("MessageList", () => {
  test("renders", () => {
    render(
      <Provider store={store}>
        <MessageList />
      </Provider>
    );
    const linkElement = screen.getByTestId(/MESSAGE-LIST-TEST-ID/i);
    expect(linkElement).toBeInTheDocument();
  });
});
