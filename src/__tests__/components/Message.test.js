import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { render, screen } from "@testing-library/react";
import Message from "../../components/Message";
import { Provider } from "react-redux";
import configureStore from "../../store/configureStore";
import { message } from "../../__mocks__/message";

const store = configureStore();

describe("MessageList", () => {
  test("renders", () => {
    render(
      <Provider store={store}>
        <Message message={message} />
      </Provider>
    );
    const linkElement = screen.getByTestId(/MESSAGE-TEST-ID/i);
    expect(linkElement).toBeInTheDocument();
  });
});
