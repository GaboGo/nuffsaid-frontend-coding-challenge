import {
  stopMessages,
  clearMessages,
  addMessage,
  removeMessage,
} from "../../actions/messages";
import { render } from "@testing-library/react";
import MessageActions from "../../components/MessageActions";
import { Provider } from "react-redux";
import configureStore from "../../store/configureStore";
import { message } from "../../__mocks__/message";

const store = configureStore();

render(
  <Provider store={store}>
    <MessageActions />
  </Provider>
);

describe("Actions", () => {
  test("stop messages", () => {
    expect(store.getState().messages.isApiStarted).toEqual(true);
    store.dispatch(stopMessages());
    expect(store.getState().messages.isApiStarted).toEqual(false);
  });

  test("add a message and then clear all messages", () => {
    store.dispatch(addMessage(message));
    expect(store.getState().messages.list.length).toBeGreaterThan(0);
    store.dispatch(clearMessages());
    expect(store.getState().messages.list.length).toEqual(0);
  });

  test("add a message and then remove it", () => {
    store.dispatch(addMessage(message));
    expect(store.getState().messages.list.length).toBeGreaterThan(0);
    store.dispatch(removeMessage(store.getState().messages.list[0].id));
    expect(store.getState().messages.list.length).toEqual(0);
  });
});
